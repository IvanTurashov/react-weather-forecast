# react-weather-forecast
React's weather forecast application using OpenWeatherMap API.
[Demo](https://ivanturashov.github.io/react-weather-forecast/)

## Getting started
Instructions for running and using the application.

### Prerequisites
You need to install (example of software that I used).

- node v8.9.4
- npm v6.1.0

Tested in browsers: Chrome, Mozilla, Chrome Android.
### Installing and start

```
npm i
npm run dev
```

Application will run on `localhost:3000`.

### Production

```
npm run build
```

Build will be located in the directory `./dist`.