/**
 * Created by ivan on 03.06.18.
 */

import React from 'react';
import ReactDom from 'react-dom';
import App from './components/App';

ReactDom.render(<App />, document.getElementById('app'));